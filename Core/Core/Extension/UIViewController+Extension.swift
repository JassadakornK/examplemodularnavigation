//
//  UIViewController+Extension.swift
//  Core
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit

public extension UIViewController {
    class func topViewController() -> UIViewController? {
        return self.topViewControllerWithRootViewController(rootViewController: UIApplication.shared.windows.first(where: {$0.isKeyWindow})?.rootViewController)
    }
    
    class func topViewControllerWithRootViewController(rootViewController: UIViewController?) -> UIViewController? {
        
        if rootViewController is UITabBarController {
            let control = rootViewController as! UITabBarController
            return self.topViewControllerWithRootViewController(rootViewController: control.selectedViewController)
        } else if rootViewController is UINavigationController {
            let control = rootViewController as! UINavigationController
            return self.topViewControllerWithRootViewController(rootViewController: control.visibleViewController)
        } else if let control = rootViewController?.presentedViewController {
            return self.topViewControllerWithRootViewController(rootViewController: control)
        }
        
        return rootViewController
        
    }
}
