import UIKit

public protocol BindableViewModel {
    associatedtype ViewModelType
    
    var viewModel: ViewModelType! { get set }
    func bindViewModel()
}

extension BindableViewModel where Self: UIViewController {
    mutating public func bind(to viewModel: ViewModelType) {
        self.viewModel = viewModel
        self.loadViewIfNeeded()
        self.bindViewModel()
    }
}

extension BindableViewModel where Self: UITableViewCell {
    mutating public func bind(to viewModel: ViewModelType) {
        self.viewModel = viewModel
        self.bindViewModel()
    }
}

extension BindableViewModel where Self: UIView {
    mutating public func bind(to viewModel: ViewModelType) {
        self.viewModel = viewModel
        self.bindViewModel()
    }
}
