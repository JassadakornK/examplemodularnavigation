import Foundation
import UIKit
import RxSwift

protocol SceneCoordinatorType {
    @discardableResult
    func transition(type: SceneTransitionType) -> Observable<Void>
}


public class SceneCoordinator: SceneCoordinatorType {
    
    private weak var window: UIWindow!
    private var currentViewController: UIViewController {
        return UIViewController.topViewController() ??  UIViewController()
    }
    
    public init(window: UIWindow) {
        self.window = window
    }
    
    public init() {
        
    }
    
    @discardableResult
    public func transition(type: SceneTransitionType) -> Observable<Void> {
        
        let result = PublishSubject<Void>()
        
        switch type {
            
        case .root(let scene):
            
            let viewController = scene.viewController()
            window.rootViewController = viewController
            window.makeKeyAndVisible()
    
            result.onCompleted()
            
            break
            
        case .modal(let scene, let animated):
            
            let viewController = scene.viewController()
            self.currentViewController.present(viewController, animated: animated) {
                result.onCompleted()
            }
            break
            
        case .push(let scene, let animated):
            
            let viewController = scene.viewController()
            
            if let navigationController = currentViewController.navigationController {
                
                navigationController.pushViewController(viewController, animated: animated)
                result.onCompleted()
                
            } else {
                fatalError("navigationController nil")
            }
            
            break
            
        case .backToRootAndPush(let scene, let animated):
            
            let viewController = scene.viewController()
            if let navigationController = currentViewController.navigationController {
                currentViewController.navigationController?.popToRootViewController(animated: animated)
                navigationController.pushViewController(viewController, animated: animated)
                result.onCompleted()
            } else {
                currentViewController.dismiss(animated: animated, completion: {
                    if let navigationController = self.currentViewController.navigationController {
                        navigationController.pushViewController(viewController, animated: animated)
                    } else {
                        fatalError("navigationController nil")
                    }
                })
            }
            break
        case .pop(let animated):
            
            if let navigationController = currentViewController.navigationController {
                
                navigationController.popViewController(animated: animated)
                result.onCompleted()
                
            } else {
                fatalError("navigationController nil")
            }
            
            break
            
        case .dismiss(let animated):
            
            if let presentingViewController = currentViewController.presentingViewController {
                
                presentingViewController.dismiss(animated: animated) { 
                    result.onNext(())
                    result.onCompleted()
                }
                
            } else {
                fatalError("presentingViewController nil")
            }
            
            break
        }
        
        return result.asObservable()
    }
}
