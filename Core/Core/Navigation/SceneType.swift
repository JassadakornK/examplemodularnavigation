//
//  SceneType.swift
//  Data
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit

public protocol SceneType {
    func viewController() -> UIViewController
}
