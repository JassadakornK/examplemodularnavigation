import Foundation

public enum SceneTransitionType {
    case root(scene: SceneType)
    case push(scene: SceneType, animated: Bool)
    case backToRootAndPush(scene: SceneType, animated: Bool)
    case modal(scene: SceneType, animated: Bool)
    case pop(animated: Bool)
    case dismiss(animated: Bool)
}
