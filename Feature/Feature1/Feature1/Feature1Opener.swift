//
//  Feature1Opener.swift
//  Data
//
//  Created by Jassadakorn Ketkaew on 3/26/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import Foundation

public enum Feature1Opener {
    case backAndPushSeemoreFeature2(id: String)
    case backAndPushDetailFeature2(id: String)
}
