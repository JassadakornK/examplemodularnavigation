//
//  Feature1DetailViewModel.swift
//  Feature1
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import Foundation
import Data

public protocol Feature1DetailViewModel: class {
    var input: Feature1DetailViewModelInput { get }
    var output: Feature1DetailViewModelOutput { get }
}

public protocol Feature1DetailViewModelInput: class {
    
}

public protocol Feature1DetailViewModelOutput: class {
    
}

final public class Feature1DetailViewModelImpl: Feature1DetailViewModel, Feature1DetailViewModelInput, Feature1DetailViewModelOutput {
    public var input: Feature1DetailViewModelInput { return self }
    public var output: Feature1DetailViewModelOutput { return self }
    
    public init(){}
}
