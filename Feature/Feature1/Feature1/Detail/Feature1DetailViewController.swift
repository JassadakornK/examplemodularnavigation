//
//  Feature1DetailViewController.swift
//  Feature1
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit
import Core

final public class Feature1DetailViewController: UIViewController, BindableViewModel {
    public var viewModel: Feature1DetailViewModel!
    public var opener: ((Feature1Opener)->Void)?
    
    @IBAction func backAndPushToFeature2Detail(_ sender: Any) {
        opener?(.backAndPushDetailFeature2(id: "1"))
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print("deinit from feature1 detail page")
    }
    
    public func bindViewModel() {
        
    }
}
