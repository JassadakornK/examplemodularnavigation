//
//  Feature1SeemoreViewModel.swift
//  Feature1
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import Foundation

public protocol Feature1SeemoreViewModel: class {
    var input: Feature1SeemoreViewModelInput { get }
    var output: Feature1SeemoreViewModelOutput { get }
}

public protocol Feature1SeemoreViewModelInput: class {
    
}

public protocol Feature1SeemoreViewModelOutput: class {
    
}

final public class Feature1SeemoreViewModelImpl: Feature1SeemoreViewModel, Feature1SeemoreViewModelInput, Feature1SeemoreViewModelOutput {
    public var input: Feature1SeemoreViewModelInput { return self }
    public var output: Feature1SeemoreViewModelOutput { return self }
    
    public init(){}
}


