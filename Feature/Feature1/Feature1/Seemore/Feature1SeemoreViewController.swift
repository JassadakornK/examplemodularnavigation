//
//  Feature1SeemoreViewController.swift
//  Feature1
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit
import Data
import Core

final public class Feature1SeemoreViewController: UIViewController, BindableViewModel {
    public var viewModel: Feature1SeemoreViewModel!
    public var opener: ((Feature1Opener)->Void)?
    
    @IBAction func backAndPushToFeature2Detail(_ sender: Any) {
        opener?(.backAndPushSeemoreFeature2(id: "1"))
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func bindViewModel() {
        
    }
}
