//
//  Feature2DetailViewController.swift
//  Feature2
//
//  Created by Jassadakorn Ketkaew on 3/24/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit
import Data
import Core

final public class Feature2DetailViewController: UIViewController, BindableViewModel {
    public var viewModel: Feature2DetailViewModel!
    public var opener: ((Feature2Opener)->Void)?
    @IBAction func backAndPushToFeature1Detail(_ sender: Any) {
        opener?(.backAndPushDetailFeature1(id: "1"))
    }
    
    deinit {
        print("deinit from feature2detail")
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func bindViewModel() {
        
    }
}
