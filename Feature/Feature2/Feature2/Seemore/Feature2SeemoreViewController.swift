//
//  Feature2SeemoreViewController.swift
//  Feature2
//
//  Created by Jassadakorn Ketkaew on 3/24/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit
import Data
import Core

final public class Feature2SeemoreViewController: UIViewController, BindableViewModel {
    public var viewModel: Feature2SeemoreViewModel!
    public var opener: ((Feature2Opener)->Void)?
    @IBAction func backAndPushToFeature1Seemore(_ sender: Any) {
        opener?(.backAndPushSeemoreFeature1(id: "1"))
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public func bindViewModel() {
        
    }
}
