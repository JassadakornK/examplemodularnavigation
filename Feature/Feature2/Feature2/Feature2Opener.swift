//
//  Feature2Opener.swift
//  Data
//
//  Created by Jassadakorn Ketkaew on 3/27/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import Foundation

public enum Feature2Opener {
    case backAndPushSeemoreFeature1(id: String)
    case backAndPushDetailFeature1(id: String)
}
