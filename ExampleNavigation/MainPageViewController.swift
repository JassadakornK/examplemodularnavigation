//
//  MainPageViewController.swift
//  ExampleNavigation
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController {
    
    private var viewModel: MainPageViewModel {
        return MainPageViewModelImpl()
    }
    
    @IBAction func pushToFeature1Seemore(_ sender: Any) {
        viewModel.input.pushToFeature1Seemore()
    }
    
    @IBAction func modalToFeature1Detail(_ sender: Any) {
        viewModel.input.modalToFeature1Detail()
    }
    
    @IBAction func pushToFeature2Seemore(_ sender: Any) {
        viewModel.input.pushToFeature2Seemore()
    }
    
    @IBAction func modalToFeature2Detail(_ sender: Any) {
        viewModel.input.modalToFeature2Detail()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
