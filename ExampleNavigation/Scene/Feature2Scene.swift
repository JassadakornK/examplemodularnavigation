//
//  Feature2Scene.swift
//  ExampleNavigation
//
//  Created by Jassadakorn Ketkaew on 3/24/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import UIKit
import Data
import Core
import Feature2
import Feature1

enum Feature2Scene {
    case feature2Seemore(Feature2SeemoreViewModel)
    case feature2Detail(Feature2DetailViewModel)
    
    var storyBoard: UIStoryboard {
        switch self {
        case .feature2Seemore, .feature2Detail:
            return UIStoryboard(name: "Feature2", bundle: Bundle(for: Feature2SeemoreViewController.self))
        }
    }
    
    var identifier: String {
        switch self {
        case .feature2Seemore:
            return "Feature2SeemoreViewController"
            case .feature2Detail:
            return "Feature2DetailViewController"
        }
    }
}

extension Feature2Scene: SceneType {
    func viewController() -> UIViewController {
        switch self {
        case .feature2Seemore(let viewModel):
            var vc: Feature2SeemoreViewController = self.storyBoard.instantiateViewController(identifier: self.identifier)
            vc.opener = feature2Opener()
            vc.bind(to: viewModel)
            return vc
        case .feature2Detail(let viewModel):
            var vc: Feature2DetailViewController = self.storyBoard.instantiateViewController(identifier: self.identifier)
            vc.opener = feature2Opener()
            vc.bind(to: viewModel)
            return vc
        }
    }
    
    private func feature2Opener() -> ((Feature2Opener) -> Void)? {
        return { opener in
            switch opener {
            case .backAndPushSeemoreFeature1(let id):
                self.backAndPushSeemoreFeature1(id)
            case .backAndPushDetailFeature1(let id):
                self.backAndPushDetailFeature1(id)
            }
        }
    }
    
    private func backAndPushSeemoreFeature1(_ id: String) {
        let viewModel: Feature1SeemoreViewModel = Feature1SeemoreViewModelImpl()
        let scene: SceneType = Feature1Scene.feature1Seemore(viewModel) //where to go
        let type: SceneTransitionType = .backToRootAndPush(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
    private func backAndPushDetailFeature1(_ id: String) {
        let viewModel: Feature1DetailViewModel = Feature1DetailViewModelImpl()
        let scene: SceneType = Feature1Scene.feature1Detail(viewModel) //where to go
        let type: SceneTransitionType = .backToRootAndPush(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
}
