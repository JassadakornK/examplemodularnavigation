import UIKit
import Data
import Core
import Feature1
import Feature2

enum Feature1Scene {
    case feature1Seemore(Feature1SeemoreViewModel)
    case feature1Detail(Feature1DetailViewModel)
}

//Create VC and binding view model
extension Feature1Scene: SceneType {
    
    //MARK: Inbound
    var storyBoard: UIStoryboard {
        switch self {
        case .feature1Seemore, .feature1Detail:
            return UIStoryboard(name: "Feature1", bundle: Bundle(for: Feature1SeemoreViewController.self))
        }
    }
    
    var identifier: String {
        switch self {
        case .feature1Seemore:
            return "Feature1SeemoreViewController"
        case .feature1Detail:
            return "Feature1DetailViewController"
        }
    }
    
    func viewController() -> UIViewController {
        switch self {
        case .feature1Seemore(let viewModel):
            var vc: Feature1SeemoreViewController = self.storyBoard.instantiateViewController(identifier: self.identifier)
            vc.opener = feature1Opener()
            vc.bind(to: viewModel)
            return vc
        case .feature1Detail(let viewModel):
            var vc: Feature1DetailViewController = self.storyBoard.instantiateViewController(identifier: self.identifier)
            vc.opener = feature1Opener()
            vc.bind(to: viewModel)
            return vc
        }
    }
    
    //MARK: Outbound
    private func feature1Opener() -> ((Feature1Opener) -> Void)? {
        return { opener in
            switch opener {
            case .backAndPushSeemoreFeature2(let id):
                self.backAndPushSeemoreFeature2(id)
            case .backAndPushDetailFeature2(let id):
                self.backAndPushDetailFeature2(id)
            }
        }
    }
    
    private func backAndPushSeemoreFeature2(_ id: String) {
        let viewModel: Feature2SeemoreViewModel = Feature2SeemoreViewModelImpl()
        let scene: SceneType = Feature2Scene.feature2Seemore(viewModel) //where to go
        let type: SceneTransitionType = .backToRootAndPush(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
    private func backAndPushDetailFeature2(_ id: String) {
        let viewModel: Feature2DetailViewModel = Feature2DetailViewModelImpl()
        let scene: SceneType = Feature2Scene.feature2Detail(viewModel) //where to go
        let type: SceneTransitionType = .backToRootAndPush(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
}

