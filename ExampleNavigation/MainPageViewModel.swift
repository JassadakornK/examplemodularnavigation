//
//  MainPageViewModel.swift
//  ExampleNavigation
//
//  Created by Jassadakorn Ketkaew on 3/22/20.
//  Copyright © 2020 Jassadakorn Ketkaew. All rights reserved.
//

import Foundation
import Data
import Core
import Feature1
import Feature2

public protocol MainPageViewModel: class {
    var input: MainPageViewModelInput { get }
    var output: MainPageViewModelOutput { get }
}

public protocol MainPageViewModelInput: class {
    func pushToFeature1Seemore()
    func modalToFeature1Detail()
    func pushToFeature2Seemore()
    func modalToFeature2Detail()
}

public protocol MainPageViewModelOutput: class {
    
}

public final class MainPageViewModelImpl: MainPageViewModel, MainPageViewModelInput, MainPageViewModelOutput {
    public var input: MainPageViewModelInput { return self }
    public var output: MainPageViewModelOutput { return self }
    
    public init() {}
    
    public func pushToFeature1Seemore() {
        let viewModel: Feature1SeemoreViewModel = Feature1SeemoreViewModelImpl()
        let scene: SceneType = Feature1Scene.feature1Seemore(viewModel) //where to go
        let type: SceneTransitionType = .push(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
    public func modalToFeature1Detail() {
        let viewModel: Feature1DetailViewModel = Feature1DetailViewModelImpl()
        let scene: SceneType = Feature1Scene.feature1Detail(viewModel) //where to go
        let type: SceneTransitionType = .modal(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
    public func pushToFeature2Seemore() {
        let viewModel: Feature2SeemoreViewModel = Feature2SeemoreViewModelImpl()
        let scene: SceneType = Feature2Scene.feature2Seemore(viewModel) //where to go
        let type: SceneTransitionType = .push(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
    
    public func modalToFeature2Detail() {
        let viewModel: Feature2DetailViewModel = Feature2DetailViewModelImpl()
        let scene: SceneType = Feature2Scene.feature2Detail(viewModel) //where to go
        let type: SceneTransitionType = .modal(scene: scene, animated: true) //how to go
        let coordinator: SceneCoordinator = SceneCoordinator()
        coordinator.transition(type: type) //go!
    }
}
